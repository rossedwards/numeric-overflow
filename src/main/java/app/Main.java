package app;

import java.math.BigInteger;

public class Main {

    private static int threshold = 1000;
    private Integer val;

    public Main(BigInteger amount) {
        if (amount == null || amount.signum() < 0) {
            throw new IllegalArgumentException();
        }
        val = amount.intValue();
        BigInteger intified = new BigInteger("" + val);
        if (!intified.equals(amount)) {
            throw new IllegalArgumentException();
        }
    }

    public static void main(String[] args) {

        Main amount = new Main(new BigInteger("999"));

        if (!approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Main(new BigInteger("2000"));
        if (approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public static boolean approval(Main amount){
        int amnt = amount.val;
           if (amnt >= threshold) {
               return true;
           }
        return false;
   }

}
