import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import app.Main;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@DisplayName("Usability unit tests")
public class appSpec {

    private BigInteger one = new BigInteger("1");

    @Test
    public void ThousandsNeedsApproval() {
        boolean res = Main.approval(new Main(new BigInteger("1000")));
        assertTrue(res, "1000 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        boolean res = Main.approval(new Main(new BigInteger("500")));
        assertFalse(res, "500 does not need approval");
    }

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        try {
            Main.approval(new Main(new BigInteger("" + Integer.MAX_VALUE).add(one)));
            fail();
        } catch (IllegalArgumentException e) {}
    }


    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        try {
            Main.approval(new Main(new BigInteger("" + Integer.MIN_VALUE).subtract(one)));
            fail();
        } catch (IllegalArgumentException e) {}
    }
}
