import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import app.Main;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        boolean res = Main.approval(new Main(new BigInteger(""+(2147483647+1))));
        assertTrue(res, "Bigger than int max size needs approval" );
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        boolean res = Main.approval(new Main(new BigInteger("" + (-2147483647-1))));
        assertTrue(res, "Less than int min size needs approval");
    }
}
